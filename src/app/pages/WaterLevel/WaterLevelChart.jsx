import React from "react";
import PropTypes from "prop-types";
import Chart from "chart.js";
import { Card, Spin, Table } from "antd";
import Text from "antd/lib/typography/Text";
import * as ChartAnnotation from "chartjs-plugin-annotation";

const WaterLevelChart = ({ waterLevel, initData }) => {
  const waterLevelRef = React.createRef();
  const [chart, setChart] = React.useState();
  const [chartReady, setChartReady] = React.useState(false);
  const [tableData, setTableData] = React.useState([]);
  React.useEffect(() => {
    var waterLevelChart = new Chart(waterLevelRef.current, {
      type: "line",
      data: {
        labels: initData.timeOfOpenValve,
        datasets: [
          {
            label: "Water Level",
            data: initData.height,
            backgroundColor: "rgb(0,255,255, .8)",
            borderColor: "rgb(0,255,255, .8)",
            borderWidth: 0,
            pointRadius: 0,
            hitRadius: 40,
          },
        ],
      },
      options: {
        plugins: [ChartAnnotation],
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: "Time of Dispense",
              },
            },
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: false,
                labelString: "Water Level",
              },
              ticks: {
                max: 100,
                maxRotation: 0,
                beginAtZero: true,
              },
            },
          ],
        },

        legend: {
          display: false,
        },
        annotation: {
          drawTime: "beforeDatasetsDraw",
          events: ["click"],
          annotations: [
            {
              drawTime: "afterDatasetsDraw",
              type: "line",
              id: "low",
              scaleID: "y-axis-0",
              value: 25,
              borderColor: "transparent",
              borderWidth: 0.1,
              mode: "horizontal",
              fill: true,
              label: {
                backgroundColor: "rgb(255,0,0, .7)",
                content: `Extremely Low`,
                enabled: true,
                xAdjust: -300,
                yAdjust: 20,
              },
            },
            {
              drawTime: "afterDatasetsDraw",
              type: "line",
              id: "medium",
              scaleID: "y-axis-0",
              value: 50,
              borderColor: "transparent",
              borderWidth: 0.1,
              mode: "horizontal",
              fill: true,
              label: {
                backgroundColor: "rgb(0,128,0, 1)",
                content: `Stable`,
                enabled: true,
                xAdjust: -300,
              },
            },
            {
              drawTime: "afterDatasetsDraw",
              type: "line",
              id: "high",
              scaleID: "y-axis-0",
              borderColor: "transparent",
              value: 100,
              mode: "horizontal",
              fill: true,
              label: {
                backgroundColor: "rgb(255,0,0, .7)",
                content: `Extremely High`,
                enabled: true,
                xAdjust: -300,
                yAdjust: 40,
              },
            },
            {
              drawTime: "afterDatasetsDraw",
              id: "hline",
              type: "line",
              mode: "horizontal",
              scaleID: "y-axis-0",
              value: initData.height[initData.height.length - 1],
              borderColor: "rgb(255,255,0, 1)",
              borderWidth: 2,
              label: {
                backgroundColor: "rgb(204,204,0, 1)",
                content: `Current Water Level: ${
                  initData.height[initData.height.length - 1]
                }%`,
                enabled: true,
                xAdjust: 400,
              },
            },
            {
              //extremely high
              type: "box",
              display: true,
              drawTime: "beforeDatasetsDraw",
              yScaleID: "y-axis-0",
              borderColor: "transparent",
              yMax: 100,
              yMin: 75,
              backgroundColor: "rgb(255,0,0, .1)",
            },
            {
              //medium
              type: "box",
              display: true,
              drawTime: "beforeDatasetsDraw",
              yScaleID: "y-axis-0",
              borderColor: "transparent",
              yMax: 75,
              yMin: 25,
              backgroundColor: "rgb(50,205,50, .1)",
            },
            {
              //extremely low
              type: "box",
              borderColor: "transparent",
              display: true,
              drawTime: "beforeDatasetsDraw",
              yScaleID: "y-axis-0",
              yMax: 25,
              yMin: 0,
              backgroundColor: "rgb(255,0,0, .1)",
            },
          ],
        },
      },
    });
    setChart(waterLevelChart);
    setChartReady(true);
    return () => {
      waterLevelChart.destroy();
      setChartReady(false);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const addData = (chart, waterLevel) => {
    const { height, timeOfOpenValve } = waterLevel;

    if (height === 0 && timeOfOpenValve === "0") return;

    chart.annotation.elements.hline.options.value = height;
    chart.annotation.elements.hline.options.label.content = `Current Water Level: ${height}`;
    chart.data.labels.push(timeOfOpenValve);
    chart.data.datasets.forEach((dataset) => {
      dataset.data.push(height);
    });
    chart.update(0);
  };
  React.useEffect(() => {
    if (chartReady && waterLevel) {
      addData(chart, waterLevel[waterLevel.length - 1]);
    }
  }, [chartReady, waterLevel, chart]);
  React.useEffect(() => {
    const data =
      initData.height.length > 0
        ? initData.height.map((value, index) => {
            const heightTime = initData.timeOfOpenValve[index];
            return {
              key:
                heightTime +
                "-" +
                value +
                Math.random().toString(36).substr(2, 9),
              timeOfOpenValve: heightTime,
              height: value,
            };
          })
        : null;
    setTableData(data);
  }, [initData]);
  console.log(tableData);
  return (
    <Card
      bordered={false}
      style={{
        padding: "10px",
        height: "100%",
        width: "100%",
        borderRadius: "10px",
        boxShadow: "0 4px 8px lightgray",
      }}
      title={<Text>Water Level</Text>}
    >
      <canvas id="water-level" ref={waterLevelRef} height="100" />
      <Card title="Summary of Water Level over time">
        {tableData.length > 0 ? (
          <Table
            dataSource={tableData}
            scroll={{ y: 300 }}
            pagination={{ pageSize: 50 }}
            columns={columns}
          />
        ) : (
          <Spin />
        )}
      </Card>
    </Card>
  );
};

const columns = [
  {
    title: "Time",
    dataIndex: "timeOfOpenValve",
  },
  {
    title: "Height",
    dataIndex: "height",
  },
];

WaterLevelChart.propTypes = {
  waterLevel: PropTypes.arrayOf(
    PropTypes.shape({
      height: PropTypes.number,
      timeOfOpenValve: PropTypes.string,
    })
  ),
  initData: PropTypes.shape({
    height: PropTypes.arrayOf(PropTypes.number),
    timeOfOpenValve: PropTypes.arrayOf(PropTypes.string),
  }),
};

export default React.memo(WaterLevelChart);

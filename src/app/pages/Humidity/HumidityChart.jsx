import React from "react";
import PropTypes from "prop-types";
import Chart from "chart.js";
import { Card, Spin, Table } from "antd";
import Text from "antd/lib/typography/Text";

const columns = [
  {
    title: "Time",
    dataIndex: "time",
  },
  {
    title: "Temperature",
    dataIndex: "temperature",
  },
];

const HumidityChart = ({ tempData, initData }) => {
  const humidityRef = React.createRef();
  const [chart, setChart] = React.useState();
  const [chartReady, setChartReady] = React.useState(false);
  const [tableData, setTableData] = React.useState([]);
  function addData(chart, toAdd) {
    const { temperature, time } = toAdd;
    if (temperature === 0 && time === "0") return;
    chart.data.labels.push(time);
    chart.data.datasets.forEach((dataset) => {
      dataset.data.push(temperature);
    });
    chart.update();
  }

  // function removeData(chart) {
  //     chart.data.labels.pop();
  //     chart.data.datasets.forEach((dataset) => {
  //         dataset.data.pop();
  //     });
  //     chart.update();
  // }
  React.useEffect(() => {
    var humidityChart = new Chart(humidityRef.current, {
      type: "line",
      data: {
        labels: initData.time,
        datasets: [
          {
            label: "Humidity",
            data: initData.temperature,
            fill: false,
            backgroundColor: "rgba(255, 99, 132, 1)",
            borderColor: "rgba(255, 99, 132, 1)",
            borderWidth: 1,
          },
        ],
      },
      options: {
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: "Time",
              },
            },
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: false,
                labelString: "Humidity",
              },
              ticks: {
                maxRotation: 0,
                beginAtZero: true,
              },
            },
          ],
        },
        legend: {
          display: false,
        },
      },
    });
    setChart(humidityChart);
    setChartReady(true);
    return () => {
      humidityChart.destroy();
      setChartReady(false);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  React.useEffect(() => {
    if (chartReady) {
      addData(chart, tempData[tempData.length - 1]);
    }
  }, [chartReady, tempData, chart]);

  React.useEffect(() => {
    const data =
      initData.temperature.length > 0
        ? initData.temperature.map((value, index) => {
            const temperatureTime = initData.time[index];
            return {
              key:
                temperatureTime +
                "-" +
                value +
                Math.random().toString(36).substr(2, 9),
              time: temperatureTime,
              temperature: value,
            };
          })
        : null;
    setTableData(data);
  }, [initData]);
  return (
    <Card
      bordered={false}
      style={{
        padding: "10px",
        width: "100%",
        borderRadius: "10px",
        boxShadow: "0 4px 8px lightgray",
      }}
      title={<Text>Humidity</Text>}
    >
      <canvas id="humidity-canvas" ref={humidityRef} height="100" />
      <Card.Meta
        style={{ marginBottom: "10px" }}
        title={<Text>Summary</Text>}
      />
      <Card>
        {tableData.length > 0 ? (
          <Table
            columns={columns}
            scroll={{ y: 300 }}
            pagination={{ pageSize: 50 }}
            key="id"
            dataSource={tableData}
          />
        ) : (
          <Spin />
        )}
      </Card>
    </Card>
  );
};

HumidityChart.propTypes = {
  tempData: PropTypes.arrayOf(
    PropTypes.shape({
      temperature: PropTypes.number,
      time: PropTypes.string,
    })
  ),
  initData: PropTypes.shape({
    temperature: PropTypes.arrayOf(PropTypes.number),
    time: PropTypes.arrayOf(PropTypes.string),
  }),
};

export default React.memo(HumidityChart);

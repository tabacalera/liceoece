import { Layout, Row, Col } from "antd";
import React from "react";
import { connect } from "react-redux";
import ErrorBoundary from "../../ErrorBoundary";
import HumidityChart from "../Humidity/HumidityChart";
import WaterLevelChart from "../WaterLevel/WaterLevelChart";
import Weight from "../Weight/Weight";
import WeightChart from "../Weight/WeightChart";

const { Header, Content, Footer } = Layout;

const LandingPage = () => {
  const [waterLevel, setWaterLevel] = React.useState([
    {
      height: 0,
      timeOfOpenValve: "0",
    },
  ]);
  const [tempData, setTempData] = React.useState([
    {
      temperature: 0,
      time: "0",
    },
  ]);

  // eslint-disable-next-line no-unused-vars
  const [initHumidityData, setInitHumidityData] = React.useState({
    temperature: [12, 15, 18, 12, 15, 18, 12, 15, 18, 12, 15, 18, 12, 15, 18],
    time: [
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
    ],
  });

  // eslint-disable-next-line no-unused-vars
  const [initWaterLevelData, setInitWaterLevelData] = React.useState({
    height: [12, 15, 18, 12, 15, 18, 12, 15, 18, 12, 15, 18, 12, 15, 13],
    timeOfOpenValve: [
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
    ],
  });

  // eslint-disable-next-line no-unused-vars
  const [initWeightData, setInitWeightData] = React.useState({
    weight: [12, 15, 18, 12, 15, 18, 12, 15, 18, 12, 15, 18, 12, 15, 13],
    time: [
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
      "2012-10-10",
    ],
  });
  React.useEffect(() => {
    setTimeout(() => {
      setWaterLevel((prevState) => [
        ...prevState,
        {
          height: 20,
          timeOfOpenValve: "2012-10-10",
        },
      ]);
    }, 1000);
    setTimeout(() => {
      setWaterLevel((prevState) => [
        ...prevState,
        {
          height: 23,
          timeOfOpenValve: "2012-10-10",
        },
      ]);
    }, 2000);
    setTimeout(() => {
      setWaterLevel((prevState) => [
        ...prevState,
        {
          height: 21,
          timeOfOpenValve: "2012-10-10",
        },
      ]);
    }, 3000);

    setTimeout(() => {
      setTempData((prevState) => [
        ...prevState,
        {
          temperature: 20,
          time: "2012-10-10",
        },
      ]);
    }, 1000);
    setTimeout(() => {
      setTempData((prevState) => [
        ...prevState,
        {
          temperature: 25,
          time: "2012-10-10",
        },
      ]);
    }, 2000);
    setTimeout(() => {
      setTempData((prevState) => [
        ...prevState,
        {
          temperature: 31,
          time: "2012-10-10",
        },
      ]);
    }, 3000);
  }, []);
  return (
    <ErrorBoundary>
      <Layout>
        <Header></Header>
        <Content>
          <Row
            gutter={[32, 32]}
            style={{ marginTop: "50px" }}
            justify="center"
            align="center"
          >
            <Col span={20}>
              <Weight />
              <Row gutter={[32, 32]}>
                <Col span={24}>
                  <WeightChart initData={initWeightData} />
                </Col>
              </Row>
              <Row gutter={[32, 32]}>
                <Col span={24}>
                  <WaterLevelChart
                    waterLevel={waterLevel}
                    initData={initWaterLevelData}
                  />
                </Col>
              </Row>
              <Row gutter={[32, 32]}>
                <Col span={24}>
                  <HumidityChart
                    tempData={tempData}
                    initData={initHumidityData}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Content>
        <Footer></Footer>
      </Layout>
    </ErrorBoundary>
  );
};

export default connect(null, null)(LandingPage);

import { DashboardRepository } from "../../domain/repositories/DashboardRepository";
import { db } from "./firebase/firebase";
const waterLevel = 28;
const foodLevel = 26;
export class DashboardRepositoryImpl implements DashboardRepository {
  async GetInitData(payload: string): Promise<any> {
    // try {
    //   // const today = moment(new Date()).format("YYYY-MM-DD")
    //   const snap = await database.ref(`/${payload}`).once("value");
    //   if (snap.val() === null) {
    //     return { message: "empty" };
    //   } else {
    //     const data = Object.values(snap.val());
    //     const keys = Object.keys(snap.val());
    //     const new_data = data.map((value: any, index) => {
    //       return { key: keys[index], ...value };
    //     });
    //     return { initData: new_data, message: "success" };
    //   }
    // } catch (err) {
    //   return { message: "error" };
    // }
  }
  async ListenForChanges(payload: any): Promise<any> {
    // const { callback, dataType } = payload;
    // const today = moment(new Date()).format("YYYY-MM-DD");
    // const dataChanges = database.ref(`/${dataType}/${today}`);
    // return dataChanges.on("value", (snap) => {
    //   if (snap.exists()) {
    //     const data = Object.values(snap.val());
    //     const keys = Object.keys(snap.val());
    //     const new_data = data.map((value: any, index) => {
    //       return { key: keys[index], ...value };
    //     });
    //     callback({ initData: new_data, message: "success" });
    //   } else {
    //     return { message: "error" };
    //   }
    // });
  }

  async getData(payload: any): Promise<any> {
    const data = localStorage.getItem(payload.name) || "";
    if (data) {
      const datum = JSON.parse(data);
      if (payload.name === "water_level") {
        const filteredData = datum.filter(
          (data: any) => data.reading <= waterLevel
        );
        return { status: true, data: filteredData };
      } else if (payload.name === "food_level") {
        const filteredData = datum.filter(
          (data: any) => data.reading <= foodLevel
        );
        return { status: true, data: filteredData };
      } else {
        return { status: true, data: datum };
      }
    } else {
      console.log("called");
      try {
        const snap = await db
          .collection(payload.name)
          .orderBy("time")
          .limit(500)
          .get();
        if (!snap.empty) {
          const data = snap.docs.map((docSnap) => {
            return { id: docSnap.id, ...docSnap.data() };
          });
          localStorage.setItem(payload.name, JSON.stringify(data));
          if (payload.name === "water_level") {
            const filteredData = data.filter(
              (data: any) => data.reading <= waterLevel
            );
            return { status: true, data: filteredData };
          } else if (payload.name === "food_level") {
            const filteredData = data.filter(
              (data: any) => data.reading <= foodLevel
            );
            return { status: true, data: filteredData };
          } else {
            return { status: true, data };
          }
        }
      } catch (err) {
        console.log(err.message);
        return { status: false, message: err.message };
      }
    }
  }
}

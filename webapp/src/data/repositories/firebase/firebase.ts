import "firebase/database";
import "firebase/firestore";
import firebase from "firebase/app";

// const firebaseConfig = {
//   apiKey: "AIzaSyDdr_ZXNhjggBfaORcdUNof-COKDkfgn9Y",
//   authDomain: "liceoece-92074.firebaseapp.com",
//   databaseURL: "https://liceoece-92074.firebaseio.com",
//   projectId: "liceoece-92074",
//   storageBucket: "liceoece-92074.appspot.com",
//   messagingSenderId: "442704869836",
//   appId: "1:442704869836:web:afe8d28c7e15b12ad98569",
// };
const firebaseConfig = {
  apiKey: "AIzaSyAA7QBcvUMdToYE9EYe9g7KCEZv65GizLg",
  authDomain: "attendance-system-9599b.firebaseapp.com",
  projectId: "attendance-system-9599b",
  storageBucket: "attendance-system-9599b.appspot.com",
  messagingSenderId: "303944219533",
  appId: "1:303944219533:web:b056165313c999afd01bc4",
};
const otherApp = firebase.initializeApp(firebaseConfig, "attendance-system");
const database = otherApp.database();
const db = otherApp.firestore();
export { database, db };

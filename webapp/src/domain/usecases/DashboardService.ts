import moment from "moment";
import { DashboardRepository } from "../repositories/DashboardRepository";

const waterHeight = 28;
const foodHeight = 26;
export class DashboardService implements DashboardRepository {
  dashboardRepo: DashboardRepository;

  constructor(ir: DashboardRepository) {
    this.dashboardRepo = ir;
  }

  async GetInitData(payload: any): Promise<any> {
    return await this.dashboardRepo.GetInitData(payload);
  }

  async ListenForChanges(payload: any): Promise<any> {
    return await this.dashboardRepo.ListenForChanges(payload);
  }

  // const read = Math.round(datum.reading * 100) / 100
  // const percentage = ((27 - read) / 27) * 100
  // return {
  //   date: moment.unix(datum.time).format("YYYY-MM-DD"),
  //   time: moment.unix(datum.time).format("hh:mm:ss A"),
  //   reading: read,
  //   volume: Math.round(percentage * 100) / 100,
  //   key: moment.unix(datum.time).format("YYYY-MM-DD-hh:mm:ss A")
  // };

  async getData(payload: any): Promise<any> {
    const response = await this.dashboardRepo.getData(payload);
    let newArr: any = [];
    let chartData: any = {};
    if (response && response.status) {
      newArr = response.data.map((datum: any) => {
        if (payload.name === "water_level") {
          const read = Math.round(datum.reading * 100) / 100;
          const volume = (waterHeight - read) * 10 * 15;
          return {
            date: moment.unix(datum.time).format("YYYY-MM-DD"),
            time: moment.unix(datum.time).format("hh:mm:ss A"),
            reading: read,
            volume: Math.round(volume * 100) / 100,
            key:
              moment.unix(datum.time).format("YYYY-MM-DD-hh:mm:ss A") +
              Math.random().toString(36).substring(7),
          };
        } else if (payload.name === "food_level") {
          const read = Math.round(datum.reading * 100) / 100;
          const volume = (foodHeight - read) * 10 * 15;
          return {
            date: moment.unix(datum.time).format("YYYY-MM-DD"),
            time: moment.unix(datum.time).format("hh:mm:ss A"),
            reading: read,
            volume: Math.round(volume * 100) / 100,
            key:
              moment.unix(datum.time).format("YYYY-MM-DD-hh:mm:ss A") +
              Math.random().toString(36).substring(7),
          };
        } else {
          return {
            date: moment.unix(datum.time).format("YYYY-MM-DD"),
            time: moment.unix(datum.time).format("hh:mm:ss A"),
            reading: Math.round(datum.reading * 100) / 100,
          };
        }
      });
      chartData["labels"] = response.data.map((datum: any) => {
        return moment.unix(datum.time).format("YYYY-MM-DD hh:mm:ss A");
      });
      chartData["readings"] = response.data.map((datum: any, index: number) => {
        if (payload.name === "water_level") {
          const read = Math.round(datum.reading * 100) / 100;
          const percentage = ((waterHeight - read) / waterHeight) * 100;
          return Math.round(percentage * 100) / 100;
        } else if (payload.name === "food_level") {
          const read = Math.round(datum.reading * 100) / 100;
          const percentage = ((foodHeight - read) / foodHeight) * 100;
          return Math.round(percentage * 100) / 100;
        } else {
          return parseInt(datum.reading);
        }
      });
      // const distributedData = [...newArr].slice().sort().reduceRight((acc: any, val: any, i: any) => {
      //     return val.reading % 2 === 0 ? [...acc, val] : [val, ...acc];
      //   }, []);
      //   console.log("ha", distributedData)

      return { status: true, summary: newArr, chart: chartData };
    } else {
      return { status: false, data: newArr };
    }
  }
}

export interface DashboardRepository {
  GetInitData(payload: any): Promise<any>;
  ListenForChanges(payload: any): Promise<any>;
  getData(payload: any): Promise<any>;
}

import React from "react";
import PropTypes from "prop-types";
import Chart from "chart.js";
import { Button, Card, Col, Row, Spin, Table } from "antd";
import Text from "antd/lib/typography/Text";
import * as ChartAnnotation from "chartjs-plugin-annotation";
import {
  getData,
  GetInitData,
  ListenForChanges,
} from "../../redux/Dashboard/Dashboard.actions";
import { connect } from "react-redux";

const WaterLevelChart = ({ waterLevel }) => {
  const [show, setShow] = React.useState(false);
  const waterLevelRef = React.createRef();
  // const [chart, setChart] = React.useState();
  // const [chartReady, setChartReady] = React.useState(false);
  const [tableData, setTableData] = React.useState([
    {
      key: "",
      height: 0,
      time: "",
    },
  ]);
  const [initData, setInitData] = React.useState({
    readings: [],
    labels: [],
    isReady: false,
  });

  // function initChartData(chart, toAdd) {
  //   const { labels, readings } = toAdd;
  //   chart.data.labels = labels;
  //   chart.data.datasets.forEach((dataset) => {
  //     // console.log(dataset)
  //     dataset.data = readings;
  //   });
  //   chart.update(0);
  // }

  // const addData = (chart, waterLevel) => {
  //   const { height, time } = waterLevel;

  //   if (height === 0 && time === "0") return;

  //   chart.annotation.elements.hline.options.value = height;
  //   chart.annotation.elements.hline.options.label.content = `Current Water Level: ${height}`;
  //   chart.data.labels.push(time);
  //   chart.data.datasets.forEach((dataset) => {
  //     dataset.data.push(height);
  //   });
  //   chart.update(0);
  // };
  // const setInitWaterLevel = async () => {
  //   let tempData = {
  //     height: [],
  //     time: [],
  //   };
  //   const response = await GetInitData("waterlevel");

  //   if (response.message === "success") {
  //     const { initData } = response;
  //     setTableData(initData);
  //     initData.forEach((item) => {
  //       if (item.height && item.time) {
  //         const height = (27 - item.height) / 27;
  //         const heightPercentage = height / 100
  //         tempData.height.push(heightPercentage);
  //         tempData.time.push(item.time);
  //       }
  //     });
  //     setInitData({
  //       ...tempData,
  //       isReady: true,
  //     });
  //   } else if (response.message === "empty") {
  //     setInitData({
  //       ...initData,
  //       isReady: true,
  //     });
  //   } else {
  //     message.info("An error occured");
  //   }
  // };
  // React.useEffect(() => {
  //   // get temperature within one hour before now
  //   // setInitWaterLevel();
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  React.useEffect(() => {
    let waterLevelChart = null;
    if (initData.isReady) {
      waterLevelChart = new Chart(waterLevelRef.current, {
        type: "line",
        data: {
          labels: initData.labels,
          datasets: [
            {
              label: "Water Level",
              data: initData.readings,
              backgroundColor: "rgb(0,255,255, .8)",
              borderColor: "rgb(0,255,255, .8)",
              borderWidth: 0,
              pointRadius: 0,
              hitRadius: 40,
            },
          ],
        },
        options: {
          plugins: [ChartAnnotation],
          scales: {
            xAxes: [
              {
                display: false,
                scaleLabel: {
                  display: true,
                  labelString: "Time of Dispense",
                },
              },
            ],
            yAxes: [
              {
                display: true,
                scaleLabel: {
                  display: true,
                  labelString: "Water Level",
                },
                ticks: {
                  max: 100,
                  maxRotation: 0,
                  beginAtZero: true,
                  callback: function (a, b, c) {
                    return a + "%";
                  },
                },
              },
            ],
          },

          legend: {
            display: true,
          },
          annotation: {
            drawTime: "beforeDatasetsDraw",
            events: ["click"],
            annotations: [
              {
                drawTime: "afterDatasetsDraw",
                type: "line",
                id: "low",
                scaleID: "y-axis-0",
                value: 25,
                borderColor: "transparent",
                borderWidth: 0.1,
                mode: "horizontal",
                fill: true,
                label: {
                  backgroundColor: "rgb(255,0,0, .7)",
                  content: `Extremely Low`,
                  enabled: true,
                  xAdjust: -300,
                  yAdjust: 20,
                },
              },
              {
                drawTime: "afterDatasetsDraw",
                type: "line",
                id: "medium",
                scaleID: "y-axis-0",
                value: 50,
                borderColor: "transparent",
                borderWidth: 0.1,
                mode: "horizontal",
                fill: true,
                label: {
                  backgroundColor: "rgb(0,128,0, 1)",
                  content: `Stable`,
                  enabled: true,
                  xAdjust: -300,
                },
              },
              {
                drawTime: "afterDatasetsDraw",
                type: "line",
                id: "high",
                scaleID: "y-axis-0",
                borderColor: "transparent",
                value: 100,
                mode: "horizontal",
                fill: true,
                label: {
                  backgroundColor: "rgb(255,0,0, .7)",
                  content: `Extremely High`,
                  enabled: true,
                  xAdjust: -300,
                  yAdjust: 40,
                },
              },
              {
                drawTime: "afterDatasetsDraw",
                id: "hline",
                type: "line",
                mode: "horizontal",
                scaleID: "y-axis-0",
                value: initData.readings[initData.readings.length - 1],
                borderColor: "rgb(255,255,0, 1)",
                borderWidth: 2,
                label: {
                  backgroundColor: "rgb(204,204,0, 1)",
                  content: `Current Water Level: ${
                    initData.readings[initData.readings.length - 1]
                  }%`,
                  enabled: true,
                  xAdjust: 400,
                },
              },
              {
                //extremely high
                type: "box",
                display: true,
                drawTime: "beforeDatasetsDraw",
                yScaleID: "y-axis-0",
                borderColor: "transparent",
                yMax: 100,
                yMin: 70,
                backgroundColor: "rgb(255,0,0, .1)",
              },
              {
                //medium
                type: "box",
                display: true,
                drawTime: "beforeDatasetsDraw",
                yScaleID: "y-axis-0",
                borderColor: "transparent",
                yMax: 70,
                yMin: 20,
                backgroundColor: "rgb(50,205,50, .1)",
              },
              {
                //extremely low
                type: "box",
                borderColor: "transparent",
                display: true,
                drawTime: "beforeDatasetsDraw",
                yScaleID: "y-axis-0",
                yMax: 20,
                yMin: 0,
                backgroundColor: "rgb(255,0,0, .1)",
              },
            ],
          },
        },
      });
      // setChart(waterLevelChart);
      // setChartReady(true);
    }
    return () => {
      waterLevelChart && waterLevelChart.destroy();
      // setChartReady(false);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initData.isReady]);
  // const listenForWaterLevelChanges= async () => {
  //   await ListenForChanges({dataType: 'waterlevel', callback: (data)=> {
  //     if(data.message==="success"){
  //       const datum = data.initData
  //       if(datum.length === tableData.length)return;
  //       const toAdd = datum[datum.length-1]
  //       const {time, height, date} = toAdd
  //       if(time && height && date){
  //         setTableData(prevState=> [...prevState, toAdd])
  //         addData(chart, {height, time})

  //       }
  //     }

  //   }})
  // }
  const getInitData = async () => {
    const response = await getData({ name: "water_level" });
    if (response.status) {
      setTableData(response.summary);
      // initChartData(chart, response.chart);
      setInitData({
        ...response.chart,
        isReady: true,
      });
    }
  };
  React.useEffect(() => {
    getInitData();
    // listenForWaterLevelChanges()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Card
      bordered={false}
      style={{
        padding: "10px",
        height: "100%",
        width: "100%",
        borderRadius: "10px",
        boxShadow: "0 4px 8px lightgray",
      }}
      title={
        <Row justify="space-between">
          <Col>
            <Text>Water Level</Text>
          </Col>
          <Col>
            <Button
              type="primary"
              style={{ borderRadius: 20 }}
              onClick={() => setShow((prevState) => !prevState)}
            >
              Show Summary
            </Button>
          </Col>
        </Row>
      }
      loading={!initData.isReady}
    >
      <canvas id="water-level" ref={waterLevelRef} height="100" />
      {show ? (
        <Card
          title="Summary of Water Level over time"
          loading={!initData.isReady}
        >
          {tableData.length > 0 ? (
            <Table
              dataSource={tableData}
              scroll={{ x: 500, y: 500 }}
              pagination={{ pageSize: 50 }}
              columns={columns}
            />
          ) : (
            <Spin />
          )}
        </Card>
      ) : null}
    </Card>
  );
};

const columns = [
  {
    title: "Date",
    dataIndex: "date",
  },
  {
    title: "Time",
    dataIndex: "time",
  },
  {
    title: "Centimeters",
    dataIndex: "reading",
  },
  {
    title: "Volume(cubic centimeter)",
    dataIndex: "volume",
  },
];

WaterLevelChart.propTypes = {
  waterLevel: PropTypes.arrayOf(
    PropTypes.shape({
      height: PropTypes.number,
      time: PropTypes.string,
    })
  ),
  initData: PropTypes.shape({
    height: PropTypes.arrayOf(PropTypes.number),
    time: PropTypes.arrayOf(PropTypes.string),
  }),
};
// const mapStateToProps = (state) => ({
//   classes: state.classes.classes,
//   user: state.protectedReducer.user,
// });
const mapDispatchToprops = (dispatch) => ({
  GetInitData: (payload) => dispatch(GetInitData(payload)),
  ListenForChanges: (payload) => dispatch(ListenForChanges(payload)),
});

export default connect(null, mapDispatchToprops)(React.memo(WaterLevelChart));

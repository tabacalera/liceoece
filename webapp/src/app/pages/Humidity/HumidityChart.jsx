import React from "react";
import PropTypes from "prop-types";
import Chart from "chart.js";
import { Button, Card, Col, Divider, Row, Table } from "antd";
import Text from "antd/lib/typography/Text";
import { getData } from "../../redux/Dashboard/Dashboard.actions";
import moment from "moment";
const columns = [
  {
    title: "Date",
    dataIndex: "date",
    width: 100,
  },
  {
    title: "Time",
    dataIndex: "time",
    width: 100,
  },
  {
    title: "Humidity",
    dataIndex: "reading",
    width: 100,
  },
];

const HumidityChart = () => {
  const [show, setShow] = React.useState(false);
  const humidityRef = React.createRef();
  const [chart, setChart] = React.useState();
  const [chartReady, setChartReady] = React.useState(false);
  const [tableData, setTableData] = React.useState([]);
  // function addData(chart, toAdd) {
  //   const { reading, time } = toAdd;
  //   if (reading === 0 && time === "0") return;
  //   chart.data.labels.push(time);
  //   chart.data.datasets.forEach((dataset) => {
  //     dataset.data.push(reading);
  //   });
  //   chart.update(0);
  // }

  function initChartData(chart, toAdd) {
    const { labels, readings } = toAdd;
    chart.data.labels = labels;
    chart.data.datasets.forEach((dataset) => {
      dataset.data = readings;
    });
    chart.update(0);
  }

  // function removeData(chart) {
  //     chart.data.labels.pop();
  //     chart.data.datasets.forEach((dataset) => {
  //         dataset.data.pop();
  //     });
  //     chart.update();
  // }
  React.useEffect(() => {
    var humidityChart = new Chart(humidityRef.current, {
      type: "line",
      data: {
        labels: [],
        datasets: [
          {
            label: "Humidity",
            data: [],
            fill: false,
            backgroundColor: "rgba(255, 99, 132, 1)",
            borderColor: "rgba(255, 99, 132, 1)",
            borderWidth: 0,
            pointRadius: 0,
            hitRadius: 40,
          },
        ],
      },
      options: {
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: "Time",
              },
              ticks: {
                callback: function (a, b, c) {
                  const unixDate = moment(a).unix();

                  return moment.unix(unixDate).format("hh:mm:ss A");
                },
              },
            },
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: "Humidity",
              },
              ticks: {
                maxRotation: 0,
                beginAtZero: true,
                callback: function (a, b, c) {
                  return a + "%";
                },
              },
            },
          ],
        },
        legend: {
          display: false,
        },
      },
    });
    setChart(humidityChart);
    setChartReady(true);
    // return () => {
    //   humidityChart.destroy();
    //   setChartReady(false);
    // };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const getInitData = async () => {
    const response = await getData({ name: "humidity" });
    if (response.status) {
      console.log(response);
      setTableData(response.summary);
      initChartData(chart, response.chart);
      // addData(chart, response.data)
    } else {
      setTableData([]);
    }
  };
  React.useEffect(() => {
    if (chartReady) {
      getInitData();
      // addData(chart, tempData[tempData.length - 1]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartReady]);

  // React.useEffect(() => {
  //   const data =
  //     initData.temperature.length > 0
  //       ? initData.temperature.map((value, index) => {
  //           const temperatureTime = initData.time[index];
  //           return {
  //             key:
  //               temperatureTime +
  //               "-" +
  //               value +
  //               Math.random().toString(36).substr(2, 9),
  //             time: temperatureTime,
  //             temperature: value,
  //           };
  //         })
  //       : null;
  //       console.log(data)
  //   // setTableData(data);
  // }, [initData]);
  return (
    <Card
      bordered={false}
      style={{
        padding: "10px",
        width: "100%",
        borderRadius: "10px",
        boxShadow: "0 4px 8px lightgray",
      }}
      title={
        <Row justify="space-between">
          <Col>
            <Text>Humidity</Text>
          </Col>
          <Col>
            <Button
              type="primary"
              style={{ borderRadius: 20 }}
              onClick={() => setShow((prevState) => !prevState)}
            >
              Show Summary
            </Button>
          </Col>
        </Row>
      }
    >
      <canvas id="humidity-canvas" ref={humidityRef} height="100" />
      <Divider />

      {show ? (
        <>
          <Card.Meta
            style={{ marginBottom: "10px" }}
            title={<Text>Summary</Text>}
          />
          <Card>
            <Table
              columns={columns}
              scroll={{ x: 500, y: 500 }}
              sticky
              pagination={{ pageSize: 50 }}
              key="id"
              dataSource={tableData}
            />
          </Card>
        </>
      ) : null}
    </Card>
  );
};

HumidityChart.propTypes = {
  tempData: PropTypes.arrayOf(
    PropTypes.shape({
      temperature: PropTypes.number,
      time: PropTypes.string,
    })
  ),
  initData: PropTypes.shape({
    temperature: PropTypes.arrayOf(PropTypes.number),
    time: PropTypes.arrayOf(PropTypes.string),
  }),
};

export default React.memo(HumidityChart);

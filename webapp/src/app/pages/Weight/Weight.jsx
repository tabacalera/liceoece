import React from "react";
import { Col, Modal, Form, Input, Row, Select, Typography, Card } from "antd";
import { SendOutlined, EditOutlined } from "@ant-design/icons";
const { Text, Title } = Typography;
const { Option } = Select;

const Weight = (props) => {
  const [weightForm] = Form.useForm();
  const [values, setValues] = React.useState({
    weight: "",
    weightInputShow: false,
    units: "kg",
  });
  const showModal = () => {
    setValues({ ...values, weightInputShow: true });
  };
  const onFinish = () => {
    console.log(values);
  };
  const handleChange = (data) => {
    setValues({
      ...values,
      weight: data.target.value,
    });
  };
  const handleSelectChange = (data) => {
    setValues({
      ...values,
      units: data,
    });
  };
  return (
    <Row gutter={[32, 32]}>
      <Modal
        centered
        visible={values.weightInputShow}
        footer={null}
        onCancel={() => setValues({ ...values, weightInputShow: false })}
      >
        <Form layout="vertical" form={weightForm}>
          <Form.Item label="Weight:" name="weight" required>
            <Input.Group compact>
              <Input
                name="weight"
                size="large"
                style={{ width: "70%" }}
                placeholder="Weight"
                suffix={
                  <SendOutlined
                    onClick={onFinish}
                    style={{ color: "blue", fontSize: "15px" }}
                  />
                }
                onChange={handleChange}
                onPressEnter={onFinish}
              />
              <Select
                defaultValue="kg"
                style={{ width: "30%" }}
                size="large"
                onChange={handleSelectChange}
              >
                <Option value="lbs">Lbs</Option>
                <Option value="kg">Kilogram</Option>
                <Option value="grams">grams</Option>
              </Select>
            </Input.Group>
          </Form.Item>
        </Form>
      </Modal>
      <Col>
        <Row justify="center" align="middle">
          <Card
            bordered={false}
            actions={[
              <EditOutlined
                onClick={showModal}
                style={{ color: "#1568b4", fontSize: "20px" }}
              />,
            ]}
            style={{
              padding: "10px",
              width: "100%",
              borderRadius: "10px",
              boxShadow: "0 4px 8px lightgray",
            }}
            title={<Text>Feeds</Text>}
          >
            {/* <Card.Meta
                                description={<Title style={{marginTop: "10px"}}>38 Kg</Title>}
                                title={<Text type="secondary" style={{fontSize: "12px"}}>Current Weight:</Text>}
                             /> */}
            <Row gutter={[30, 0]} align="middle">
              <Col>
                <Text type="secondary">Current Weight:</Text>
              </Col>
              <Col>
                <Title style={{ marginTop: "10px" }}>38 Kg</Title>
              </Col>
            </Row>
          </Card>
        </Row>
      </Col>
    </Row>
  );
};

Weight.propTypes = {};

export default React.memo(Weight);

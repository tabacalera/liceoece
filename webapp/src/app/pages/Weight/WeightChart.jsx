import React from "react";
import PropTypes from "prop-types";
import { Card, Spin, Table } from "antd";
import Text from "antd/lib/typography/Text";
import Chart from "chart.js";
import * as ChartAnnotation from "chartjs-plugin-annotation";

const WeightChart = () => {
  const weightFeedRef = React.createRef();
  // eslint-disable-next-line no-unused-vars
  const [chart, setChart] = React.useState();
  // eslint-disable-next-line no-unused-vars
  const [chartReady, setChartReady] = React.useState(false);
  // eslint-disable-next-line no-unused-vars
  const [tableData, setTableData] = React.useState([]);
  // console.log(initData);
  React.useEffect(() => {
    var weightFeedChart = new Chart(weightFeedRef.current, {
      type: "line",
      data: {
        labels: [],
        datasets: [
          {
            label: "Weight",
            data: [],
            fill: false,
            backgroundColor: "rgba(255, 99, 132, 1)",
            borderColor: "rgba(255, 99, 132, 1)",
            borderWidth: 1,
          },
        ],
      },
      options: {
        plugins: [ChartAnnotation],
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: "Time of Dispense",
              },
            },
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: "Weight",
              },
              ticks: {
                maxRotation: 0,
                beginAtZero: true,
              },
            },
          ],
        },
        legend: {
          display: false,
        },
      },
    });
    setChart(weightFeedChart);
    setChartReady(true);
    return () => {
      weightFeedChart.destroy();
      setChartReady(false);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // React.useEffect(() => {
  //   const data =
  //     initData.weight.length > 0
  //       ? initData.weight.map((value, index) => {
  //           const weightTime = initData.time[index];
  //           return {
  //             key:
  //               weightTime +
  //               "-" +
  //               value +
  //               Math.random().toString(36).substr(2, 9),
  //             time: weightTime,
  //             weight: value,
  //           };
  //         })
  //       : null;
  //   setTableData(data);
  // }, [initData]);

  return (
    <Card
      bordered={false}
      style={{
        padding: "10px",
        height: "100%",
        width: "100%",
        borderRadius: "10px",
        boxShadow: "0 4px 8px lightgray",
      }}
      title={<Text>Weight of Feeds over time</Text>}
    >
      <canvas id="feed-weight" ref={weightFeedRef} height="100"></canvas>
      <Card title="Summary of the weight of feeds over time">
        {tableData.length > 0 ? (
          <Table
            scroll={{ y: 300 }}
            pagination={{ pageSize: 50 }}
            dataSource={tableData}
            columns={columns}
          />
        ) : (
          <Spin />
        )}
      </Card>
    </Card>
  );
};

const columns = [
  {
    title: "Time",
    dataIndex: "time",
  },
  {
    title: "Weight",
    dataIndex: "weight",
  },
];

WeightChart.propTypes = {
  initData: PropTypes.shape({
    weight: PropTypes.arrayOf(PropTypes.number),
    time: PropTypes.arrayOf(PropTypes.string),
  }),
};

export default WeightChart;

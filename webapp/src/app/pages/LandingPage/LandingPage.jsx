import { Layout, Row, Col, Tabs } from "antd";
import Title from "antd/lib/typography/Title";
import React from "react";
import { connect } from "react-redux";
import ErrorBoundary from "../../ErrorBoundary";
import FoodLevel from "../FoodLevel/FoodLevel";
import HumidityChart from "../Humidity/HumidityChart";
import Temperature from "../Temperature/Temperature";
import WaterLevelChart from "../WaterLevel/WaterLevelChart";
// import WeightChart from "../Weight/WeightChart";

const { Content, Footer } = Layout;
const { TabPane } = Tabs;
const LandingPage = () => {
  return (
    <ErrorBoundary>
      <Layout>
        <Content>
          <Row gutter={[32, 32]} justify="center" align="center">
            <Col lg={20} xs={23}>
              <Row style={{ marginTop: "30px" }}>
                <Col lg={20} xs={22}>
                  <Title level={3} style={{ color: "gray" }}>
                    Automation and Monitoring for Livestock Feed Consumption in
                    Poultry Farming
                  </Title>
                </Col>
              </Row>
              <Tabs>
                <TabPane tab="Humidity" key="1">
                  <Row gutter={[32, 32]}>
                    <Col span={24}>
                      <HumidityChart />
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tab="Temperature" key="2">
                  <Row gutter={[32, 32]}>
                    <Col span={24}>
                      <Temperature />
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tab="Water Level" key="3">
                  <Row gutter={[32, 32]}>
                    <Col span={24}>
                      <WaterLevelChart />
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tab="Food Level" key="4">
                  <Row gutter={[32, 32]}>
                    <Col span={24}>
                      <FoodLevel />
                    </Col>
                  </Row>
                </TabPane>
              </Tabs>
            </Col>
          </Row>
        </Content>
        <Footer></Footer>
      </Layout>
    </ErrorBoundary>
  );
};

export default connect(null, null)(LandingPage);

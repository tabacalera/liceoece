import { DashboardRepositoryImpl } from "../../../data/repositories/DashboardRepositoryImpl";
import { DashboardService } from "../../../domain/usecases/DashboardService";

export const GetInitData = async (payload) => {
  const DashboardRepo = new DashboardRepositoryImpl();
  const dashboardService = new DashboardService(DashboardRepo);
  return await dashboardService.GetInitData(payload);
};

export const ListenForChanges = async (payload) => {
  const DashboardRepo = new DashboardRepositoryImpl();
  const dashboardService = new DashboardService(DashboardRepo);
  return await dashboardService.ListenForChanges(payload);
};

export const getData = async (payload) => {
  const DashboardRepo = new DashboardRepositoryImpl();
  const dashboardService = new DashboardService(DashboardRepo);
  return await dashboardService.getData(payload);
};
